const express = require('express');
const { randomBytes } = require('crypto');
const router = express.Router();

/* GET account data */
router.get('/:accountId/data', async function(req, res, next) {
  res.json({
      'accountId': req.params.accountId,
      'timestamp': 1111, // Date.now(),
      'data': randomBytes(24).toString('base64')
  });
});

module.exports = router;
