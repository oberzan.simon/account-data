# Infrastructure Engineering Homework

A simple service, where each change of the service goes through a CI/CD pipeline, and is automatically deployed to production.

### Create HTTP service

For implementation of this simple HTTP service I have used Node.js with Express framework. This API only has one valid route which is on /{accountId}/data

### Setup CI/CD

I have configured CI/CD pipeline using Gitlab's pipelines. In this demo CI/CD automatically triggers on each new pushed commit. The pipeline consists of 3 stages, each executing a single job:

1. Build stage builds a Docker image, tags it and pushes it to Amazon Elastic Container Registry.
2. Test stage runs a mocha test script inside the container ran from an image that was built in previous stage.
3. Depoly stage updates the Elastic Container Service to use the latest build that we pushed to ECR in our build stage.