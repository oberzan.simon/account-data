const request = require('supertest');
const app = require('../app');

describe('App', () => {
  it('has no default page', done => {
    request(app)
      .get('/')
      .expect(404, done);
  });

  it('returns correct account data', done => {
    const accId = 'SI0023481';
    request(app)
      .get(`/${accId}/data`)
      .expect(200)
      .expect(res => {
        if (!(res.body.accountId === accId))      throw new Error('wrong accountId');
        if (!(res.body.timestamp <= Date.now()))  throw new Error('wrong timestamp');
        if (!(res.body.data.length === 32))       throw new Error('data too short');
      })
      .end(done);
  });

  it('has no account index page', done => {
    const accId = 'SI0023481';
    request(app)
      .get(`/${accId}`)
      .expect(404, done);
  });
}); 
